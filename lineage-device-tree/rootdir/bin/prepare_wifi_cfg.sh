#!/vendor/bin/sh

WIFI_CHIP_TYPE=`getprop odm.wifi.chip.type`

case ${WIFI_CHIP_TYPE} in
    "4.3")
        SRC_DIR="/vendor/etc/wifi/qca6750"
        ;;
    "4.6")
        SRC_DIR="/vendor/etc/wifi/qca6490"
        ;;
    *)
        SRC_DIR="/vendor/etc/wifi"
esac

VERSION_STR=`grep version ${SRC_DIR}/version.txt`
setprop ro.vendor.wifi.cfg_version ${VERSION_STR:8}
