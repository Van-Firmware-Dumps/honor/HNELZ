#
# Honor HDY keyboard configuration file
#

# device is external
device.internal = 0
# device has mic led
device.hasMicLed = 1
# device is touch keyboard
device.isTouchKeyboard = 1
# device has app switch
device.appSwitch = 1
